<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link rel="stylesheet" type="text/css" href="style/succes.css">
        <link href='https://fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <?php 
            include("ressources/menu.php"); 
        ?>
        
        <div class="match">
            Vous avez été inscrit avec succès !<br>
            <a href="/">Retour à l'accueil</a>
        </div>
    </body>
</html>