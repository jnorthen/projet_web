<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link rel="stylesheet" type="text/css" href="style/wrongCredentials.css">
        <link href='https://fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css'>
    </head>
    <body>       
        <?php
        include("ressources/menu.php");
        ?>
        <div class="match">
            Les identifiants que vous avez saisis ne sont pas corrects.<br>
            <a href="/connexion.php">Réessayer</a><br>
            <a href="/">Retour à l'accueil</a>
        </div>
    </body>
</html>