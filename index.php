<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link rel="stylesheet" type="text/css" href="style/index.css">
        <link href='https://fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css'>
        <link rel="icon" type="image/png" href="ressources/icon.png" />
        <title>RugByCar</title>
    </head>
    <body>
        <?php 
            include("ressources/menu.php"); 
            include("configuration/config.php");
        ?>
        
        <div class="match">
            Matchs programmés
        </div>
      
       
        <?php 

        $mysqli = new mysqli(SERVER, USER, PASSWD, DB_NAME);
        $mysqli->set_charset("utf8");

        $matchesParPage=12;

        $retour_total = $mysqli->query("SELECT COUNT(*) AS total FROM matches");
        $donnees_total = $retour_total->fetch_assoc();
        $total=$donnees_total['total'];

        $nombreDePages=ceil($total/$matchesParPage);

        if(isset($_GET['page'])) {
             $pageActuelle=intval($_GET['page']);

             if($pageActuelle > $nombreDePages) {
                  $pageActuelle = $nombreDePages;
             }
        } else {
            $pageActuelle = 1;
        }

        $premiereEntree = ($pageActuelle - 1) * $matchesParPage;

        $index = 0 ;
        if ($mysqli->connect_errno){
            echo "Erreur lors de la connexion" ;
        } else {
            $results = $mysqli->query("SELECT matches.date_match, matches.id_match, matches.ville, a.nom as equ_a, b.nom as equ_b, a.flag as flag_a, b.flag as flag_b FROM matches 
JOIN equipes as a ON a.id_equipe = matches.equ_a
JOIN equipes as b ON b.id_equipe = matches.equ_b ORDER BY date_match ASC LIMIT " . $premiereEntree . ", " . $matchesParPage);
            if(!$results) {
                
            } else {
                while ($ligne = $results->fetch_assoc()){  
                    $d = substr($ligne['date_match'], 8);
                    $m = substr($ligne['date_match'], 5, 2);
                    switch($m){
                        case "01" : $m = "Janvier" ; break ;
                        case "02" : $m = "Février" ; break ;
                        case "03" : $m = "Mars" ; break ;
                        case "04" : $m = "Avril" ; break ;
                        case "05" : $m = "Mai" ; break ;
                        case "06" : $m = "Juin" ; break ;
                        case "07" : $m = "Juillet" ; break ;
                        case "08" : $m = "Août" ; break ;
                        case "09" : $m = "Septembre" ; break ;
                        case "10" : $m = "Octobre" ; break ;
                        case "11" : $m = "Novembre" ; break ;
                        case "12" : $m = "Décembre" ; break ;
                        default : break;
                    }
                    
                    $url = "match.php" ;
                    $method = "GET" ;
                    
                    if (!isset($_SESSION['id']) || empty($_SESSION['id'])){
                         $url = "connexion.php" ;
                         $method = "POST" ;
                    }
                    
                    echo '
                    <div class="box">
                        <div class="box-header">
                            ' . $d . ' ' . $m . ' @ ' . $ligne['ville'] . '
                        </div>
                        
                       
                        <div class="team a">
                        <img class="flag" src="ressources/flags-mini/' . $ligne['flag_a'] . '"/><br>
                        ' . $ligne['equ_a'] . ' 
                        </div>
                        <div class="team b">
                        <img class="flag" src="ressources/flags-mini/' . $ligne['flag_b'] . '"/><br>
                        ' . $ligne['equ_b'] . '
                        </div>        
                    <form id="form' . $ligne['id_match'] . '" action="'.$url.'" method="'.$method .'">
                    <input type="hidden" name="id" value="'. $ligne['id_match'] .'"/>
                    <button class="click" type="submit" form="form'. $ligne['id_match'] .'" value="Submit">Voir les trajets</button>
                    
                    </form>
                    </div>' ;
                    
                    
                }
            }
        }

        echo '<p class="pages">';
        if ($pageActuelle != 1){
                echo '<a class="f prec" href="?page='.($pageActuelle-1).'"">‹</a>' ;
        }
        for($i = 1; $i <= $nombreDePages; $i++) {
            
             if($i == $pageActuelle) {
                 echo '<a class="pa active">' . $i . '</a>'; 
             }	
             else {
                  echo ' <a class="pa clickable" href="?page='.$i.'">'.$i.'</a> ';
             }
        }
        if ($pageActuelle != $nombreDePages){
                echo '<a class="f suiv" href="?page='.($pageActuelle+1).'"">›</a>' ;
        }
        echo '</p>';
        ?>            
         
            
    </body>
</html>