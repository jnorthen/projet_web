<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link href='https://fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="style/inscription.css">
                <link rel="icon" type="image/png" href="ressources/icon.png" />

        <title>Connexion</title>
    </head>
    <body>
      <?php include("ressources/menu.php"); ?>
        
        <div class="match">
            Connexion
        </div>
        <form action="services/SignIn.php" method="POST">
            <div class="block">
                <div class="header">
                    Entrez vos identifiants... 
                </div>
                <div class="else">
                    <input class="text" type="email" name="email" placeholder="Adresse e-mail" required/><br>
                    <input class="text"  type="password" name="password" placeholder="Mot de passe" required/><br>
                    <input class="text"  type="hidden" name="referer" value="<?php echo $_SERVER['HTTP_REFERER'] ; ?>"/>
                    <input class="text"  type="hidden" name="trajet" value="
                                                                            <?php 
                    if(!empty($_POST['id']) && isset($_POST['id'])){
                        echo $_POST['id'] ; 
                    }
                    ?>
                                                                            
                    "/>                     
                </div>
            </div>
            
            <button class="butt" type="submit">CONNEXION !</button>
        </form>
       
        </div>
    </body>
</html>

