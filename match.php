<?php
include ("configuration/config.php");

$mysqli = new mysqli(SERVER, USER, PASSWD, DB_NAME);
$mysqli->set_charset("utf8");
$match_id = $mysqli->real_escape_string($_GET['id']);
$results = $mysqli->query("SELECT matches.date_match, matches.id_match, matches.ville, a.nom as equ_a, b.nom as equ_b, a.flag as flag_a, b.flag as flag_b FROM matches JOIN equipes as a ON a.id_equipe = matches.equ_a JOIN equipes as b ON b.id_equipe = matches.equ_b WHERE id_match = " . $_GET['id']);
$match = $results->fetch_assoc();
$ville = $match['ville']; ?>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <link rel="stylesheet" type="text/css" href="style/match.css">
    <link href='https://fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css'>
    <title>
        <?php
echo $match['equ_a'] . " - " . $match['equ_b']; ?>
    </title>
    <link rel="icon" type="image/png" href="ressources/icon.png" />
</head>

<body>
    <?php
include ("ressources/menu.php");
 ?>
    <div class="content">
        <div class="match">
            <div class="team a">
                <div class="lbl x">
                    <?php
echo $match['equ_a']; ?>
                </div>
                <div class="f x">
                    <img class="flag" src="ressources/flags-normal/
<?php
echo $match['flag_a']; ?>
" />
                </div>
            </div>
            <div class="team b">
                <div class="f x">
                    <img class="flag" src="ressources/flags-normal/
<?php
echo $match['flag_b']; ?>
" />
                </div>
                <div class="lbl x">
                    <?php
echo $match['equ_b']; ?>
                </div>
            </div>
        </div>
        <div class="liste">
            <?php

if ($mysqli->connect_errno) {
	echo "Erreur lors de la connexion";
}
else {
	$add = "";
	if (isset($_SESSION['id']) && !empty($_SESSION['id'])) {
		$add = "AND trajet.id_trajet NOT IN ( SELECT id_trajet FROM covoit WHERE id_mbr = " . $_SESSION['id'] . ")";
	}

	$query = "SELECT membre.nom_mbr, membre.prenom_mbr, trajet.id_trajet, trajet.nb_place, trajet.prix, trajet.date, trajet.id_conducter FROM trajet JOIN membre ON trajet.id_conducter = membre.id_mbr WHERE trajet.id_match = " . $match_id . " " . $add . " " . "ORDER BY date ASC";
	$results = $mysqli->query($query);
	if (!$results) { // IMPROVE, when < count($results->fetch_assoc()); > first line is ommited
		echo 'Personne n\'a encore prévu de trajet pour ce match...';
		if (isset($_SESSION['id']) && !empty($_SESSION['id'])) {
			echo '
            <form id="prop" action="proposer.php" method="POST">
                <input type="hidden" name="id" value="' . $match_id . '" />
                <button class="propo" type="submit" form="prop" value="Submit">
                    Proposer un trajet pour ce match
                </button>
            </form>
            ';
		}
	}
	else {
		if (isset($_SESSION['id']) && !empty($_SESSION['id'])) {
			echo '
            <form id="prop" action="proposer.php" method="POST">
                <input type="hidden" name="id" value="' . $match_id . '" />
                <button class="propo" type="submit" form="prop" value="Submit">
                    Proposer un trajet pour ce match
                </button>
            </form>
            ';
		}

		while ($ligne = $results->fetch_assoc()) {
			$d = substr($ligne['date'], 8);
			$m = substr($ligne['date'], 5, 2);
			switch ($m) {
			case "01":
				$m = "Janvier";
				break;

			case "02":
				$m = "Février";
				break;

			case "03":
				$m = "Mars";
				break;

			case "04":
				$m = "Avril";
				break;

			case "05":
				$m = "Mai";
				break;

			case "06":
				$m = "Juin";
				break;

			case "07":
				$m = "Juillet";
				break;

			case "08":
				$m = "Août";
				break;

			case "09":
				$m = "Septembre";
				break;

			case "10":
				$m = "Octobre";
				break;

			case "11":
				$m = "Novembre";
				break;

			case "12":
				$m = "Décembre";
				break;

			default:
				break;
			}

			$p = $ligne['nb_place'];
			$query_bis = "SELECT COUNT(*) as total FROM covoit WHERE id_trajet = " . $ligne['id_trajet'];
			$results_bis = $mysqli->query($query_bis);
			$row = $results_bis->fetch_assoc();
			$nb = $ligne['nb_place'] - intval($row['total']);
			$c = "";
			$b = '';
			$box = 'box';
			$boxheader = 'box-header';
			if ($nb > 1) {
				$p = "
            <div class='places'>
                <span class='green'>
<b>
" . $nb . "
</b>
places restantes
</span>
            </div>
            ";
				$b = '
            <button type="submit" form="form' . $ligne['id_trajet'] . '" value="Submit" class="but">
                Participer
            </button>
            ';
				$b = '
            <form id="form' . $ligne['id_trajet'] . '" action="services/Participate.php" method="POST">
                <input type="hidden" name="id" value="' . $ligne['id_trajet'] . '" />
                <button class="click" type="submit" form="form' . $ligne['id_trajet'] . '" value="Submit">
                    Commander
                </button>
            </form>
            ';
			}
			else
			if ($nb == 1) {
				$p = "
            <div class='places'>
                <span class='orange'>
<b>
" . $nb . "
</b>
place restante
</span>
            </div>
            ";
				$b = '
            <button type="submit" form="form' . $ligne['id_trajet'] . '" value="Submit" class="but">
                Participer
            </button>
            ';
				$b = '
            <form id="form' . $ligne['id_trajet'] . '" action="services/Participate.php" method="POST">
                <input type="hidden" name="id" value="' . $ligne['id_trajet'] . '" />
                <button class="click" type="submit" form="form' . $ligne['id_trajet'] . '" value="Submit">
                    Commander
                </button>
            </form>
            ';
			}
			else {
				$p = "
            <div class='space'>
                COMPLET
            </div>
            ";
				$c = " complet";
				$b = '';
				$box = 'complet';
				$boxheader = 'complet-header';
			}

			if (!isset($_SESSION['id']) || empty($_SESSION['id'])) {
				$b = '
            <span style="font-size: 0.7em">
<br />
Vous devez être connecté
<br />
pour réserver un trajet
</span> ';
			} else if ($ligne['id_conducter'] == $_SESSION['id']) {
               $b = '
            <span style="font-size: 0.7em; color: #6060e1;">
        <br />
       Ceci est un de vos trajets
        </span> '; 
            }

			echo '
            <div class="' . $box . '">
                <div class="' . $boxheader . '">
                    Le
                    <b>
' . $d . ' ' . $m . '
</b> vers
                    <b>
' . $ville . '
</b>
                </div>
                <div class="left">
                    <div class="user">
                        <b>
' . $ligne['prenom_mbr'] . ' ' . $ligne['nom_mbr'] . '
</b>
                    </div>
                    ' . $p . '
                </div>
                <div class="right">
                    <div class="prix">
                        ' . number_format($ligne['prix'], 2, ',', ' ') . ' €
                    </div>
                    ' . $b . '
                </div>
            </div>
            ';
		}
	}
} ?>
        </div>
    </div>
</body>

</html>