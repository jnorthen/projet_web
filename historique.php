<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link rel="stylesheet" type="text/css" href="style/historique.css">
        <link href='https://fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css'>
        <title>Historique</title>
                <link rel="icon" type="image/png" href="ressources/icon.png" />

    </head>
    <body>
       

        <?php include("ressources/menu.php");
         if (!isset($_SESSION['id']) || empty($_SESSION['id'])){
            header("Location: /");
         }
       ?>
        <div class="content">
            
            
            
        <?php
        include("configuration/config.php");
        $mysqli = new mysqli(SERVER, USER, PASSWD, DB_NAME);
        $mysqli->set_charset("utf8");
        
    
        if ($mysqli->connect_errno){
            echo "Erreur lors de la connexion" ;
        } else {            
            $query = "SELECT covoit.id_covoit, trajet.date, covoit.id_trajet, trajet.prix, m1.nom_mbr, m1.prenom_mbr, a.nom as equ_a, b.nom as equ_b, a.flag as flag_a, b.flag as flag_b FROM matches JOIN equipes as a ON a.id_equipe = matches.equ_a JOIN equipes as b ON b.id_equipe = matches.equ_b JOIN trajet ON trajet.id_match = matches.id_match JOIN membre as m1 ON trajet.id_conducter =  m1.id_mbr JOIN covoit ON covoit.id_trajet = trajet.id_trajet JOIN membre as m2 ON covoit.id_mbr = m2.id_mbr WHERE m2.id_mbr = " . $_SESSION['id'] ;            
            $results = $mysqli->query($query);
            if(mysqli_num_rows($results) == 0) {
               echo 'Vous n\'avez réservé aucun trajet...';               
            } else { 
                while ($ligne = $results->fetch_assoc()){
                    $d = substr($ligne['date'], 8);
                    $m = substr($ligne['date'], 5, 2);
                    switch($m){
                        case "01" : $m = "Janvier" ; break ;
                        case "02" : $m = "Février" ; break ;
                        case "03" : $m = "Mars" ; break ;
                        case "04" : $m = "Avril" ; break ;
                        case "05" : $m = "Mai" ; break ;
                        case "06" : $m = "Juin" ; break ;
                        case "07" : $m = "Juillet" ; break ;
                        case "08" : $m = "Août" ; break ;
                        case "09" : $m = "Septembre" ; break ;
                        case "10" : $m = "Octobre" ; break ;
                        case "11" : $m = "Novembre" ; break ;
                        case "12" : $m = "Décembre" ; break ;
                        default : break;
                    }
                      echo '<div class="h">
                      <div class="h-header">
                        <div class="team a">
                            <div class="lbl x">
                                '.$ligne['equ_a'].'
                            </div>
                            <div class="f x">
                               <img class="flag" src="ressources/flags-normal/'.$ligne['flag_a'].'">
                            </div>                    
                        </div>
                        <div class="team b">                    
                            <div class="f x">
                               <img class="flag" src="ressources/flags-normal/'.$ligne['flag_b'].'">
                            </div> 
                            <div class="lbl x">
                                '.$ligne['equ_b'].'
                            </div>
                        </div>
                    </div>                    
                    <div class="h-content">
                      <div class="brick user">' . $ligne['prenom_mbr'] . ' ' . $ligne['nom_mbr'] . '</div>
                      <div class="brick date">' . $d . ' ' . $m . '</div>
                      <div class="brick prix">' . $ligne['prix'] . ' € TTC</div>
                      <div class="brick annuler">
                          <form action="services/Unparticipate.php" method="POST">
                              <input type="hidden" name="trajet" value="'. $ligne['id_trajet'] .'"/>
                              <button class="cancel" type="submit" value="Annuler">Annuler</button>
                          </form>
                      </div>
                    </div>  
                    </div>' ; 
                    
                      
                }
            }
        }
        ?>  
                </div>
            
        
            </div>
    </body>
</html>