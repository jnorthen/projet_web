<?php
session_start();
include("../configuration/config.php");
$mysqli = new mysqli(SERVER, USER, PASSWD, DB_NAME);
$mysqli->set_charset("utf8");
$e = "";
$p = "";
if (isset($_POST['email']) && !empty($_POST['email'])) {
    $e = $_POST['email'];
}
if (isset($_POST['password']) && !empty($_POST['password'])) {
    $p = $_POST['password'];
}
if ($mysqli->connect_errno) {
    echo "Erreur lors de la connexion";
} else {
    $query   = "SELECT * FROM membre WHERE email = '" . $e . "' AND pwd = MD5('" . $p . "') LIMIT 1";
    $results = $mysqli->query($query);
    $user    = $results->fetch_assoc();
    if (count($user) == 0) {
        // header("Location: " . $_POST['referer']);
        header("Location: ../wrongCredentials.php");        
    } else {
        $_SESSION['id']        = $user['id_mbr'];
        $_SESSION['nom']       = $user['nom_mbr'];
        $_SESSION['prenom']    = $user['prenom_mbr'];
        $_SESSION['email']     = $user['email'];
        $_SESSION['naissance'] = $user['date_naiss_mbr'];
        $_SESSION['permis']    = $user['date_permis_mbr'];
        $_SESSION['adresse']   = $user['adresse_mbr'];
        $_SESSION['voiture']   = $user['voiture_mbr'];
       
        header("Location: " . $_POST['referer']);
        
    }
}
?>