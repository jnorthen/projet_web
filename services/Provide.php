<?php
session_start();
include("../configuration/config.php");
$mysqli = new mysqli(SERVER, USER, PASSWD, DB_NAME);
$mysqli->set_charset("utf8");
/*
ini_set('SMTP','mailout.one.com');
ini_set('smtp_port','25');*/
$match  = "";
$places = "";
$prix   = "";
$jb     = "";
$mb     = "";
$ab     = "";
if (isset($_POST['match']) && !empty($_POST['match'])) {
    $match = $mysqli->real_escape_string($_POST['match']);
}
if (isset($_POST['places']) && !empty($_POST['places'])) {
    $places = $mysqli->real_escape_string($_POST['places']);
}
if (isset($_POST['prix']) && !empty($_POST['prix'])) {
    $prix = $mysqli->real_escape_string($_POST['prix']);
}
if (isset($_POST['jb']) && !empty($_POST['jb'])) {
    $jb = $mysqli->real_escape_string($_POST['jb']);
}
if (isset($_POST['mb']) && !empty($_POST['mb'])) {
    $mb = $mysqli->real_escape_string($_POST['mb']);
}
if (isset($_POST['ab']) && !empty($_POST['ab'])) {
    $ab = $mysqli->real_escape_string($_POST['ab']);
}
$query = "INSERT INTO trajet VALUES(NULL, " . $_SESSION['id'] . ", " . $match . ", " . $places . ", " . $prix . ", '', '" . $ab . "-" . $mb . "-" . $jb . "')";

if ($mysqli->connect_errno) {
    echo "Erreur lors de la connexion";
} else {
    $mysqli->query($query);
}
header("Location: " . $_SERVER['HTTP_REFERER']);
?>