<?php
session_start();
include("../configuration/config.php");
$mysqli = new mysqli(SERVER, USER, PASSWD, DB_NAME);
$mysqli->set_charset("utf8");
if ($mysqli->connect_errno) {
    echo "Erreur lors de la connexion";
} else {
    $query = "DELETE FROM trajet WHERE id_conducter = " . $_SESSION['id'] . " AND id_trajet = " . $_POST['trajet'];
    $mysqli->query($query);
    header("Location: " . $_SERVER['HTTP_REFERER']);
}
?>