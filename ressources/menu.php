<?php session_start(); ?>
<nav>
    <a href="/">
        <div class="logo">
            <img src="ressources/logo.png"/>        
        </div>   
        <div class="title">
            Rug<span class="by">by</span>Car     
        </div>
    </a>
    <div class="hello">
        <?php 
        if(isset($_SESSION['id']) && !empty($_SESSION['id'])) {
            echo "Bonjour, <b>" . $_SESSION['prenom'] . " " . $_SESSION['nom'] . "</b> !";
        }
        ?>
    </div>
    <div class="menu">
        <ul>
            <li><a href="index.php" class="menu-item">Matchs</a></li>
            
                <?php                      
                    
                if(isset($_SESSION['prenom']) && !empty($_SESSION['prenom'])) {
                    echo '<li><a href="historique.php" class="menu-item">Historique</a></li>';
                    echo '<li><a href="proposer.php" class="menu-item">Mes trajets</a></li>';
                    echo '<li><a href="services/LogOut.php" class="menu-item"><b>Deconnexion</b></a></li>';
                } else {
                    echo '<li><a href="inscription.php" class="menu-item">Inscription</a></li>';
                    echo '<li><a href="connexion.php" class="menu-item">Connexion</a></li>'; 
                } 
            ?>
        </ul>
    </div>    
</nav>
        
