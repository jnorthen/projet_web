<?php 
include("ressources/menu.php");
         if (!isset($_SESSION['id']) || empty($_SESSION['id'])){
            header("Location: /");
         }
       ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link href='https://fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="style/proposer.css">
        <link rel="icon" type="image/png" href="ressources/icon.png" />
        <title>Proposer un trajet</title>
    </head>
    <body>
      <?php
include("configuration/config.php");
?>
        
        <div class="match">
           Vos trajets
        </div>
         <?php
$id_match = 0 ;
if (isset($_POST['id']) && !empty($_POST['id'])){
    $id_match = $_POST['id'] ;
}

$mysqli = new mysqli(SERVER, USER, PASSWD, DB_NAME);
$mysqli->set_charset("utf8");
if ($mysqli->connect_errno) {
    echo "Erreur lors de la connexion";
} else {
    $query   = "SELECT trajet.id_trajet, a.nom as equ_a, b.nom as equ_b, a.flag as flag_a, b.flag as flag_b, trajet.nb_place, trajet.prix, trajet.date FROM matches JOIN trajet ON matches.id_match = trajet.id_match JOIN membre ON trajet.id_conducter = membre.id_mbr JOIN equipes as a ON a.id_equipe = matches.equ_a JOIN equipes as b ON b.id_equipe = matches.equ_b WHERE membre.id_mbr = " . $_SESSION['id'] . " ORDER BY date ASC";
    $results = $mysqli->query($query);
    if (mysqli_num_rows($results) == 0) {
        echo 'Vous n\'avez prévu aucun trajet.';
    } else {
		while ($ligne = $results->fetch_assoc()) {
             $d = substr($ligne['date'], 8);
                    $m = substr($ligne['date'], 5, 2);
                    switch($m){
                        case "01" : $m = "Janvier" ; break ;
                        case "02" : $m = "Février" ; break ;
                        case "03" : $m = "Mars" ; break ;
                        case "04" : $m = "Avril" ; break ;
                        case "05" : $m = "Mai" ; break ;
                        case "06" : $m = "Juin" ; break ;
                        case "07" : $m = "Juillet" ; break ;
                        case "08" : $m = "Août" ; break ;
                        case "09" : $m = "Septembre" ; break ;
                        case "10" : $m = "Octobre" ; break ;
                        case "11" : $m = "Novembre" ; break ;
                        case "12" : $m = "Décembre" ; break ;
                        default : break;
                    }
              echo '<div class="h">
                      <div class="h-header">
                        <div class="team a">
                            <div class="lbl x">
                                '.$ligne['equ_a'].'
                            </div>
                            <div class="f x">
                               <img class="flag" src="ressources/flags-normal/'.$ligne['flag_a'].'">
                            </div>                    
                        </div>
                        <div class="team b">                    
                            <div class="f x">
                               <img class="flag" src="ressources/flags-normal/'.$ligne['flag_b'].'">
                            </div> 
                            <div class="lbl x">
                                '.$ligne['equ_b'].'
                            </div>
                        </div>
                    </div>                    
                    <div class="h-content">
                      <div class="brick date">' . $d . ' ' . $m . '</div>
                      <div class="brick prix">' . $ligne['prix'] . ' € TTC</div>
                      <div class="brick annuler">
                          <form action="services/Delete.php" method="POST">
                              <input type="hidden" name="trajet" value="'. $ligne['id_trajet'] .'"/>
                              <button class="cancel" type="submit" value="Annuler">Annuler</button>
                          </form>
                      </div>
                    </div>  
                    </div>' ; 
        }
    }
}
?>  
        
        <div class="match-bis">
           Proposer un trajet
        </div>
        <form action="services/Provide.php" method="POST" id="form">
            <div class="block vous">
                <div class="header">
                    INFORMATIONS 
                </div>
                <div class="else">
                    Pour quel match proposez vous un trajet ?<br><br>
                    <select name="match">
                        <?php
if ($mysqli->connect_errno) {
    echo "Erreur lors de la connexion";
} else {
    $results = $mysqli->query("SELECT matches.date_match, matches.id_match, matches.ville, a.nom as equ_a, b.nom as equ_b, a.flag as flag_a, b.flag as flag_b FROM matches 
JOIN equipes as a ON a.id_equipe = matches.equ_a
JOIN equipes as b ON b.id_equipe = matches.equ_b ORDER BY matches.date_match ASC");
    if ($results) {
        while ($ligne = $results->fetch_assoc()) {
            if ($ligne['id_match'] == $id_match){
                echo '<option value="' . $ligne['id_match'] . '" required selected>
                                        
                                        ' . $ligne['equ_a'] . ' -
                                       
                                        ' . $ligne['equ_b'] . '
                                        
                                        </option>';
            } else {
                 echo '<option value="' . $ligne['id_match'] . '" required>
                                        
                                        ' . $ligne['equ_a'] . ' -
                                       
                                        ' . $ligne['equ_b'] . '
                                        
                                        </option>';
            }
           
        }
    }
}
?>     
                    </select>
                    <br><br>Pour quelle date ?<br><br>
                    <select name="jb" class="birth">
                        <?php
for ($i = 1; $i <= 31; $i++) {
    echo '<option value="' . $i . '">' . $i . '</option>';
}
?>
                    </select>                    
                    <select name="mb" class="birth">
                        <?php
for ($i = 1; $i <= 12; $i++) {
    echo '<option value="' . $i . '">' . $i . '</option>';
}
?>
                    </select>
                     <select name="ab" class="birth">
                        <?php
for ($i = 2015; $i <= 2017; $i++) {
    echo '<option value="' . $i . '">' . $i . '</option>';
}
?>
                    </select>
                    <br><br>Combien de places sont libres ?<br><br>
                    <input pattern="[0-9]" title="Seuls les chiffres sont autorisés" name="places" type="number" required/>
                    <br><br>Quel est votre prix ?<br><br>
                    <input pattern="[0123456789\.]" title="Seuls les chiffres sont autorisés" name="prix" type="number" min="1" required/> €
                </div>
            </div>
        </form>       
        <button class="butt" form="form" type="submit">PROPOSER !</button>
    </body>
</html>
