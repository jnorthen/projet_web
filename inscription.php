<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link href='https://fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="style/inscription.css">
                <link rel="icon" type="image/png" href="ressources/icon.png" />

        <title>Inscription</title>
    </head>
    <body>
      <?php include("ressources/menu.php"); ?>
        
        <div class="match">
            Inscription
        </div>
        <form action="services/SignUp.php" method="POST">
            <div class="block vous">
                <div class="header">
                    VOUS 
                </div>
                <div class="else">
                    <input class="text" type="text" name="nom" placeholder="Nom" required/><br>
                    <input class="text" type="text" name="prenom" placeholder="Prénom" required/><br>
                    <input class="text" type="email" name="email" placeholder="Adresse e-mail" required/><br>
                    <input class="text"  type="email" name="email_conf" placeholder="Confirmation adresse e-mail" required/><br>
                    <input class="text"  type="text" name="adresse" placeholder="Adresse postale" required/><br><br>
                    Date de naissace<br><br>
                    <select name="jb" class="birth">
                        <?php
                            for($i = 1 ; $i <= 31 ; $i++){
                                echo '<option value="'. $i .'">' . $i . '</option>';
                            }
                        ?>
                    </select>                    
                    <select name="mb" class="birth">
                        <?php
                            for($i = 1 ; $i <= 12 ; $i++){
                                echo '<option value="'. $i .'">' . $i . '</option>';
                            }
                        ?>
                    </select>
                     <select name="ab" class="birth">
                        <?php
                            for($i = 1900 ; $i <= 2010 ; $i++){
                                echo '<option value="'. $i .'">' . $i . '</option>';
                            }
                        ?>
                    </select><br><br>
                    Date d'obtention du permis de conduire<br><br>
                    <select name="jp" class="birth">
                        <?php
                            for($i = 1 ; $i <= 31 ; $i++){
                                echo '<option value="'. $i .'">' . $i . '</option>';
                            }
                        ?>
                    </select>                    
                    <select name="mp" class="birth">
                        <?php
                            for($i = 1 ; $i <= 12 ; $i++){
                                echo '<option value="'. $i .'">' . $i . '</option>';
                            }
                        ?>
                    </select>
                     <select name="ap" class="birth">
                        <?php
                            for($i = 1900 ; $i <= 2016 ; $i++){
                                echo '<option value="'. $i .'">' . $i . '</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>

            <div class="block vehicule">
                <div class="header">
                    PRÉFÉRENCES
                </div>
                <div class="else">
                    <input class="text" type="text" name="voiture" placeholder="Modèle de voiture" required/><br>                    
                        <?php
                            include("configuration/config.php");
                            $mysqli = new mysqli(SERVER, USER, PASSWD, DB_NAME);

                            $mysqli->set_charset("utf8");


                            if ($mysqli->connect_errno) {
                                echo "Erreur lors de la connexion" ;
                            } else {
                                $results = $mysqli->query("SELECT * FROM equipes ORDER BY nom ASC");
                                if($results) {
                                    echo '<select name="fav-team">' ;
                                    while ($ligne = $results->fetch_assoc()){  
                                        echo '<option value="' . $ligne['id_equipe'] . '">' . $ligne['nom'] . '</option>';
                                    }
                                    echo '</select>' ;
                                }
                            }
                        ?>                    
                </div>
            </div>

            <div class="block security">
                <div class="header">
                    SÉCURITÉ
                </div>
                <div class="else">
                    <input class="text" type="password" name="password" placeholder="Mot de passe" required/><br>
                    <input class="text" type="password" name="password_conf" placeholder="Confirmation du mot de passe" required/><br>
                </div>
            </div>

            <button class="butt" type="submit">S'INSCRIRE !</button>
        </form>
       
        </div>
    </body>
</html>

